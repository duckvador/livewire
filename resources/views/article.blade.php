@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="jumbotron">
            <h1 class="display-4">{{ $article->title }}</h1>
            <p class="lead">{{ $article->subtitle }}</p>
            <hr class="my-4">
            <p>{{ $article->content }}</p>
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="{{ route('home') }}" role="button">Retour</a>
            </p>
        </div>

    </div>


    </div>
@endsection
