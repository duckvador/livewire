@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('articles.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Titre</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Titre de l'article">
                            @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Sous-titre</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="subtitle" id="exampleInputPassword1"
                            placeholder="Sous-titre du l'article">
                        <small id="emailHelp" class="form-text text-muted">Brève description du titre sera utilisée pour
                            créer le slug de l’url</small>
                            @error('subtitle')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Contenu</label>
                        <textarea type="text" class="form-control @error('title') is-invalid @enderror" name="content" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Contenu de l'article"></textarea>
                            @error('content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>

                    <hr>
                    <button type="submit" class="btn btn-primary mb-2">Envoyer</button>


                </form>

            </div>
        </div>
    </div>
@endsection
