<div>@foreach ($articles as $article)

    <div wire:model="article" class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">{{$article->title}}</h5>
          <h6 class="card-subtitle mb-2 text-muted">{{$article->subtitle}}</h6>
          {{-- <p class="card-text">{{$article->content}}</p> --}}
          <a href="{{ route('article', $article->slug) }}" class="card-link">Lire la suite</a>
        </div>
      </div>
    @endforeach
</div>
