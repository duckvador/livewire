<div>
    <input wire:model.lazy="foo" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
    @if($users != null)
    <label for="">@if($users != null) Le courriel existe déjà. :( @endif</label>
    @endif
</div>