<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = Article::get();
        return view('home', compact('articles'));
    }

    public function show(Article $article){
        // $article = Article::where('slug', $slug)->firstOrFail();

        return view('article', [
            'article' => $article,
        ]);
    }
}
