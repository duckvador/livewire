<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;

class FetchArticles extends Component
{
    public function render()
    {
        return view('livewire.fetch-articles', [
            'articles' => Article::Get(),
        ]);
    }
}
